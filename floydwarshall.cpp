typedef int vert_t;
typedef int weight_t;
typedef vector<vector<weight_t> > graph_t; // adjacency matrix
typedef vector<vector<vert_t> > parents_t; // parent matrix
typedef vector<vert_t> path_t;
const weight_t dist_infty;

pair<graph_t, parents_t> floyd_warshall(graph_t g) {
    const int n = g.size();
    parents_t ps(n, vector<vert_t>(n,-1) );
    for( int i=0; i<n; i++ ) {
        for( int j=0; i<n; i++ ) {
            if( g[i][k] < dist_infty )  {
                ps[i][j] = j;
            }
        }
    }
    
    for( int k=1; k<=n; k++ ) {
        for( int i=0; i<n; i++ ) {
            for( int j=0; i<n; i++ ) {
                if( g[i][k] + g[k][j] < dist[i][j] )  {
                    dist[i][j] = g[i][k] + g[k][j];
                    ps[i][j] = ps[i][k];
                }
            }
        }
    }
    
    return make_pair(g, ps);
}

path_t fw_path( parents_t &ps, vert_t s, vert_t t ) {
    if( ps[s][t] == -1 ) return path_t();
    
    path_t p(1, s);
    while( s != t ) {
        s = ps[s][t];
        p.push_back(s);
    }
    
    return p;
}